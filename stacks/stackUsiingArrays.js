
function stack() {
    this.dataStore = [];
    this.top = 0;
    this.push = push;
    this.pop = pop;
    this.peek = peek;
    this.length = length;
    this.clear = clear;
}

//Push an element on top of stack and then increment the top
function push(itemToBeinserted) {
    this.dataStore[this.top] = itemToBeinserted;
    this.top = this.top + 1;

}

// Pop: remove & return an element from top of stack and decrement the top 
function pop() {
    if(this.top > 0) {
        this.top = this.top -1;
        return this.dataStore[this.top];
    } else {
        console.log('Stack is empty');
    }

}

// peek: return the topmost element of stck without decrementiong the top.
function peek(){
    console.log(this.dataStore[this.top - 1] || 'Peek: Stack is empty');
}

function length() {
    return this.top;
}

function clear() {
    this.top = 0;
}


var s = new stack();
s.push('BMW');
s.push('maruti');
s.push('merc');
s.push('Toyota');
console.log(`Length: ${s.length()}`)
s.peek();
s.pop();
console.log(`Length: ${s.length()}`)
s.peek();
s.clear();
console.log(`Length: ${s.length()}`);
s.peek();
s.pop();
console.log(`Length: ${s.length()}`);
s.peek();