
function stack() {
    this.dataStore = [];
    this.top = 0;
    this.push = push;
    this.pop = pop;
    this.peek = peek;
    this.length = length;
    this.clear = clear;
}

//Push an element on top of stack and then increment the top
function push(itemToBeinserted) {
    this.dataStore[this.top] = itemToBeinserted;
    this.top = this.top + 1;

}

// Pop: remove & return an element from top of stack and decrement the top 
function pop() {
    if(this.top > 0) {
        this.top = this.top -1;
        return this.dataStore[this.top];
    } else {
        console.log('Stack is empty');
    }

}

// peek: return the topmost element of stck without decrementiong the top.
function peek(){
    console.log(this.dataStore[this.top - 1] || 'Peek: Stack is empty');
}

function length() {
    return this.top;
}

function clear() {
    this.top = 0;
}

function recursionFacorial(num){
    var s = new stack();
    for(let i = 1; i <= num; i++){
        s.push(i);
    }

    var fac = 1;
    while(s.length() > 0){
        fac = fac * s.pop();
    }
    return fac;
}

console.log(`Factorial of 5: ${recursionFacorial(5)}`);
console.log(`Factorial of 4: ${recursionFacorial(4)}`);
console.log(`Factorial of 6: ${recursionFacorial(6)}`);
console.log(`Factorial of 10: ${recursionFacorial(10)}`);