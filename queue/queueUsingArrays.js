function Queue(){
    this.dataStore = [];
    this.enqueue = enqueue;
    this.dequeue = dequeue;
    this.front = front;
    this.back  = back;
    this.toString = toString;
    this.isEmpty = isEmpty;

}

function enqueue(itemToAdd){
    this.dataStore.push(itemToAdd);
}

function dequeue() {
    return this.dataStore.shift();
}

function front() {
    return this.dataStore[0];
}

function back() {
    return this.dataStore[this.dataStore.length -1];
}

function toString() {
    let str = '';
    for(let i = 0; i < this.dataStore.length; i++){
        str = str + this.dataStore[i] + ' ';
    }
    return str;
}

function isEmpty() {
    if(this.dataStore.length == 0){
        return true;
    } else {
        return false;
    }
}

let q = new Queue();

q.enqueue('India');
q.enqueue('UK');
q.enqueue('USA');
q.enqueue('Russia');
console.log(q.toString());
console.log(q.front());
console.log(q.back());
q.enqueue('Austria');
q.enqueue('UAE');
console.log(q.toString());
q.dequeue();
q.dequeue();
console.log(q.toString());
console.log(q.isEmpty());
console.log(q.front());
console.log(q.back());