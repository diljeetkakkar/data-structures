function Node(element){
    this.element = element;
    this.next = null;
} 

// find an item in linked list
function find(item) {
    var currentNode = this.head;

    while(true) {
        if(currentNode.element === item) {
            console.log(`Item Found : ${currentNode.element}`);
            return currentNode;
        }
        if(currentNode.next == null){
            console.log('No such element found');
            return -1;
        }
        currentNode = currentNode.next;
    }

}

//Insert an item into linked list 
function insert(itemTOBeInserted, item = 'Head'){
    var newNode = new Node(itemTOBeInserted);
    var currentNode = this.find(item);
    if(currentNode != -1) {
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        console.log(`Item inserted successfully: ${newNode.element}`);
    }
}

function insertInBeginning(itemTOBeInserted) {
    var newNode = new Node(itemTOBeInserted);
    newNode.next = this.head.next;
    this.head.next = newNode;
    console.log('Node Inserted successfully in beginning.');
}

function insertAtEnd(item) {
    var newNode = new Node(item);
    var currentNode = this.head;
    while(true){
        if(currentNode.next == null){
            currentNode.next = newNode;
            newNode.next = null;
            console.log('Item inserted successfully at end.');
            return;
        }
        currentNode = currentNode.next;
    }
}

//Remove item from beginning of linked list
function removeFromBeginning(){
    var currentNode = this.head;
    var nodeToBeRemoved = this.head.next;
    currentNode.next = nodeToBeRemoved.next;
    console.log('First Node Removed successfully.');
}

// Remove the last node of linked list
function removeNodeFromEnd(){
    var currentNode = this.head;

    while(true){
        if(currentNode.next.next == null) {
            currentNode.next = null;
            console.log('last node removed successfully.');
            return;
        }
        currentNode = currentNode.next;
    }
}

function findPreviousNode(itemTobeFound){
    var currentNode = this.head;

    while(currentNode.next != null && currentNode.next.element !== itemTobeFound) {
        currentNode = currentNode.next;
    }
    return currentNode;
}

function removeNode(itemToBeFound) {
    var prevNode = this.findPreviousNode(itemToBeFound);
    if(prevNode.next != null) {
        prevNode.next = prevNode.next.next;
        console.log('Item Removed successfully.');
    } else {
        console.log('Item Not Found');
    }
    
}

// Traverse and display linked list
function display(){
    var currentNode = this.head;
    console.log('Display Element of Linked List: \n');
    do {
        console.log(`Node with Element: ${currentNode.next.element}`);
        currentNode = currentNode.next;
    } while(!(currentNode.next == null))
    console.log('\n');
}

//Linked list creation
function LList (){
    this.head = new Node('Head');
    this.insert = insert;
    this.find = find;
    this.display = display;
    this.insertInBeginning = insertInBeginning;
    this.insertAtEnd = insertAtEnd;
    this.removeFromBeginning = removeFromBeginning;
    this.removeNodeFromEnd = removeNodeFromEnd; 
    this.findPreviousNode = findPreviousNode;
    this.removeNode = removeNode
}

var cars = new LList();
cars.insertInBeginning('Maruti 1');
cars.insert('Maruti 2','Maruti 1' )
cars.insert('Maruti 3','Maruti 2' )
cars.insert('Maruti 2','Maruti 5' )
cars.insert('Maruti 3','Maruti 7' )
cars.insertInBeginning('Maruti 4');
cars.display();
cars.insertAtEnd('Maruti 5');
cars.display();
cars.removeFromBeginning();
cars.display();
cars.removeNodeFromEnd();
cars.display();
cars.removeNode('Maruti 2');
cars.removeNode('Maruti 25');
cars.display();

