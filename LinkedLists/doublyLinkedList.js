function Node(element){
    this.element = element;
    this.next = null;
    this.previous = null;
}

function find(itemToBeFound) {
    var currentNode = this.head;

    while(true){
        if(currentNode.element == itemToBeFound){
            console.log(`Item found: ${currentNode.element}`);
            return currentNode;
        }
        if(currentNode.next == null){
            console.log(`${currentNode.element} : No such elment found!!`);
            return -1;
        }
        currentNode = currentNode.next;
    }
}

function insert(itemToBeInserted, itemAtPosition) {
    var newNode = new Node(itemToBeInserted);
    var currentNode = this.find(itemAtPosition);
    if(currentNode != -1){
        newNode.next = currentNode.next;
        currentNode.next = newNode;
        newNode.previous = currentNode;
        if(newNode.next !== null) newNode.next.previous = newNode;
        console.log(`Element: ${newNode.element} Item inserted successfully!! `);
    } else {
        console.log('Insertion failed');
    }

}

function remove(itemToBeRemoved) {
    var currentNode = this.find(itemToBeRemoved);
    if(currentNode.next != null){
        currentNode.previous.next =currentNode.next;
        currentNode.next.previous= currentNode.previous;
        currentNode.next = null;
        currentNode.previous = null;
    }
    if(currentNode.next == null && currentNode.previous != null){
        currentNode.previous.next = null;
        currentNode.next = null;
        currentNode.previous = null;
    }
    console.log(`${itemToBeRemoved} is removed succesfully.`);
}

function display(){
    var currentNode = this.head;
    console.log('Display Doubly Linked List: \n ');
    while(!(currentNode.next == null)){
        console.log(`Element: ${currentNode.next.element}`);
        currentNode = currentNode.next;
    }
    console.log('\n');
}

function DLList() {
    this.head = new Node('Head');
    this.find = find;
    this.insert= insert;
    this.display = display;
    this.remove = remove;
}

var cities = new DLList();
cities.insert('Agra', 'Head');
cities.insert('Ambala', 'Agra');
cities.insert('Moga', 'Ambala');
cities.insert('Goa', 'Agra');
cities.display();
cities.remove('Moga');
cities.display();
cities.remove('Agra');
cities.display();