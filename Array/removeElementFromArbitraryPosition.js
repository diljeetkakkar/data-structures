
const removeElementFromArbitraryPosition = (nums, positionToStartDeletingFrom) => {
    let numsArr = nums;
    const N = numsArr.length;
    console.log(`Original Array: ${numsArr} with length: ${numsArr.length}`)
    for(var i = positionToStartDeletingFrom -1; i < N; i++){
        numsArr[i] = numsArr[i + 1];
    }
    numsArr.length = numsArr.length - 1;
    console.log(`Updated Array: ${numsArr} with length: ${numsArr.length}`)
}

// removeElementFromArbitraryPosition([1,2,3,4,55,5,6,7,8,9], 5);

const removeElementFromArbitraryPositionMethod2 = (nums, positionToStartDeletingFrom, numberOfElementsTobeRemoved) => {
    let numsArr = nums;
    console.log(`Initial Array: ${numsArr} with length: ${numsArr.length}`)
    numsArr.splice(positionToStartDeletingFrom -1, numberOfElementsTobeRemoved);
    console.log(`Updated Array: ${numsArr} with length: ${numsArr.length}`)
}

// removeElementFromArbitraryPositionMethod2([1,2,3,4,55,66,77,5,6,7,8,9], 5,3);

const removeElementFromArbitraryPositionMethod3 = (nums, positionToStartDeletingFrom, numberOfElementsTobeRemoved) => {
    let numsArr = nums;
    var N = numsArr.length;
    if(numberOfElementsTobeRemoved > (numsArr.length - positionToStartDeletingFrom) || positionToStartDeletingFrom > numsArr.length -1 ) {
        console.log('Invalid Input Parameters');
    } else {
        console.log(`Initial Array: ${numsArr} with length: ${numsArr.length}`)
        for(var i = positionToStartDeletingFrom -1; i < N; i++){
            numsArr[i] = numsArr[i + numberOfElementsTobeRemoved];
        }
        numsArr.length = numsArr.length - numberOfElementsTobeRemoved;
        console.log(`Updated Array: ${numsArr} with length: ${numsArr.length}`)
    }

}

removeElementFromArbitraryPositionMethod3([1,2,3,4,55,66,77,5,6,7,8,9], 5,1);
removeElementFromArbitraryPositionMethod3([1,2,3,4,55,66,77,5,6,7,8,9], 22,2);
removeElementFromArbitraryPositionMethod3([1,2,3,4,55,66,77,5,6,7,8,9], 3,19);
removeElementFromArbitraryPositionMethod2([1,2,3,4,55,66,77,5,6,7,8,9], 5,5);