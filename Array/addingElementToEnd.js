// Adding elemet to the end of the array (using Array.push())

//Method 1
function addingElementsToEnd(nums, elementToBeAdded) {
    console.log(`Method 1`);
    console.log(`Esiting Array: ${nums}`);
    nums.push(elementToBeAdded);
    console.log(`New Element added: ${elementToBeAdded}`);
    console.log(`New Array: ${nums}`);
}
addingElementsToEnd([1,2,3,4,5,7,8,9], 23);

//Method 2
function addingElementsToEndMethod2(nums, elementToBeAdded){
    console.log(`Method 2`);
    console.log(`Esiting Array: ${nums}`);
    nums[nums.length] = elementToBeAdded
    console.log(`New Element added: ${elementToBeAdded}`);
    console.log(`New Array: ${nums}`);
}
addingElementsToEndMethod2([1,2,3,4,5,6,7,8], 9);