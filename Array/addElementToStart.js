
//Method1
function addElementToBeginning(nums, elementToBeAdded){
    let numArr = nums;
    console.log(`Existing Array: ${nums}`);
    for(var i = numArr.length; i >= 0; i--){
        numArr[i] = numArr[i-1];       
    }
    numArr[0] = elementToBeAdded;
    console.log(`New Array: ${numArr}`);
}

addElementToBeginning([2,3,4,5,6,7,8], 1);

//Method2
function AddElementToBeginningMethod2(nums, elementToBeAdded){
    let numArr = nums;
    console.log(`Existing Array: ${numArr}`);
    numArr.unshift(elementToBeAdded);
    console.log(`New Array: ${numArr}`);
}

AddElementToBeginningMethod2([2,3,4,5], 1);