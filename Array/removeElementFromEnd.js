

const removeElementFromEnd = (nums) => {
    console.log(`Existing Array: ${nums}`);
    nums.pop();
    console.log(`Updated Array: ${nums}`);
}

removeElementFromEnd([1,2,4,5,6]);

const removeElementFromEndMethod2 = nums => {
    console.log(`Existing Array: ${nums}`);
    nums.length = nums.length -1 ;
    console.log(`Updated Array: ${nums}`);
}

removeElementFromEndMethod2([1,2,3,4,5,6,7,8]);


