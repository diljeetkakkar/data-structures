
const addElementsToArbitraryPosition = (nums, position, elementsToBeAdded) => {
    let numsArr = nums;
    console.log(`Existing Array: ${numsArr} with length: ${numsArr.length}`);
    numsArr.splice(position, 0, ...elementsToBeAdded);
    console.log(`Updated Array: ${numsArr} with length: ${numsArr.length}`);
}

addElementsToArbitraryPosition([1,2,3,7,8,9], 3, [4,5,6]);

const addElementsToArbitraryPositionMethod2 = (nums, position, elementToBeAdded) => { 
    const N = nums.length;
    let numsArr = nums ;
    console.log(`Existing Array: ${numsArr} with length: ${numsArr.length}`);
    for(let i = N; i >= position; i--){
        numsArr[i] = numsArr[i - 1];
    }
    numsArr[position - 1] = elementToBeAdded;
    console.log(`Updated Array: ${numsArr} with length: ${numsArr.length}`);
}

addElementsToArbitraryPositionMethod2([1,2,3,4,6,7,8,9], 5, 5);