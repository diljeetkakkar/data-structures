


const removeElementFromStart = nums => {
    let numsArr = nums;
    console.log(`Existing Array: ${numsArr}  with length: ${numsArr.length}`);
    numsArr.shift();
    console.log(`Updated Array: ${numsArr}  with length: ${numsArr.length}`);
}

removeElementFromStart([9,1,2,3,4,5,6]);

const removeElementFromStartMethod2 = nums => {
    let numsArr = nums;
    console.log(`Existing Array: ${numsArr}  with length: ${numsArr.length}`);
    for(var i =0; i< numsArr.length; i++){
        numsArr[i] =numsArr[i+1];
    }
    numsArr.length = numsArr.length -1;
    console.log(`Updated Array: ${numsArr}  with length: ${numsArr.length}`);
}

removeElementFromStartMethod2([2,3,4,5,6,7,8]);