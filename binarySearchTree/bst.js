function Node(data, left, right){
    this.data = data;
    this.left = left;
    this.right = right;
    this.show = show;
}
function show() {
    return this.data;
}

function bst() {
    this.root = null;
    this.insert = insert;
    this.inOrder = inOrder;
}

function insert(data){
    let n = new Node(data, null, null);
    if(this.root == null){
        this.root = n;
    } else{
        var current = this.root;
        var parent;
        while(true){
            parent = current;
            if(data < current.data) {
                current = current.left;
                if(current == null){
                    parent.left = n;
                    break;
                }
            } else {
                current = current.right;
                if(current == null){
                    parent.right = n;
                    break;
                }
            }
        }
    }
}






//In Order tranersal of BST
function inOrder(node){
    if(node !== null){
        inOrder(node.left);
        console.log(node.show());
        inOrder(node.right);
    }
}


function preOrder(node){
    if(node == null){
        return;
    } else {
        console.log(node.show());
        preOrder(node.left);
        preOrder(node.right);
    }
}

var nums = new bst();
nums.insert(50);
nums.insert(60);
nums.insert(40);
nums.insert(30);
nums.insert(20);
nums.insert(90);
console.log('InOrder Traversal:')
inOrder(nums.root);

console.log('\n PREORDER Traversal:')
preOrder(nums.root);
